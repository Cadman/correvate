# Correvate File Zipper Applicatoin

### Running Using mvnw
Run the following code in the projects root directory
`./mvnw spring-boot:run`

### Running using Docker

**Build the Jar**
`./mvnw clean package`

**Build the Docker Image**
`docker build -t correvate-zipper .`

**Run the Docker Container**
`docker run -dp 8080:8080 correvate-zipper`

## Check that App can zip files
```
curl --location --request POST 'localhost:8080/zip-files' \
 --form 'data=@"<absolute_route_to_file>"' \
 --form 'data=@"<absolute_route_to_file>" > zipped-file.zip'
```