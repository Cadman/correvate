FROM openjdk:11.0.8

COPY target/zipper*.jar /code/zipper.jar

EXPOSE 8080

CMD java -Xmx256m -Xms160m -jar /code/zipper.jar

