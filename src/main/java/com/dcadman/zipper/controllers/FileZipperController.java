package com.dcadman.zipper.controllers;

import com.dcadman.zipper.services.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.DescriptiveResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class FileZipperController {

    private final FileStorageService fileStorageService;

    @Autowired
    public FileZipperController(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @PostMapping(value = "/zip-files", produces = "application/zip")
    @ResponseBody
    public ResponseEntity<Resource> zipFiles(@RequestParam(value = "data", required = false) List<MultipartFile> files) {
        try {
            if (isNullOrEmpty(files)) {
                return ResponseEntity.badRequest().body(new DescriptiveResource("No files provided in upload"));
            }
            fileStorageService.saveFiles(files);
            List<String> filenames = files.stream().map(MultipartFile::getOriginalFilename).collect(Collectors.toList());
            File zippedFile = fileStorageService.zipFiles(filenames);
            return ResponseEntity.ok().body(new ByteArrayResource(Files.readAllBytes(zippedFile.toPath())));
        } catch (IOException e){
            return ResponseEntity.internalServerError().body(new DescriptiveResource("Error encountered while Zipping files"));
        } finally {
            fileStorageService.clearAllFiles();
        }
    }

    private boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

}
