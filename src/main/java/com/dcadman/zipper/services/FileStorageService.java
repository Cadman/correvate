package com.dcadman.zipper.services;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class FileStorageService {

    private final Path rootLocation;
    private final FileAttribute<Set<PosixFilePermission>> DEFAULT_PERMISSIONS =
            PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-xr-x"));

    @Autowired
    public FileStorageService(@Value("${upload.directory}") String rootLocation) {
        this.rootLocation = Paths.get(rootLocation);
        createDirectory();
    }

    public void saveFiles(List<MultipartFile> files) {
        files.forEach(this::saveFile);
    }

    public void saveFile(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new RuntimeException("Failed to store empty file " + file.getOriginalFilename());
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(Objects.requireNonNull(file.getOriginalFilename())));
        } catch (IOException e) {
            throw new RuntimeException("Failed to store file " + file.getOriginalFilename(), e);
        }
    }

    public List<File> getFiles(List<String> filenames) {
        return filenames.stream().map(this::getFile).collect(Collectors.toList());
    }

    public File getFile(String filename) {
        return rootLocation.resolve(filename).toFile();
    }

    public File zipFiles(List<String> filenames) throws ZipException {
        ZipFile zipFile = new ZipFile("zipped-file.zip");
        zipFile.addFiles(getFiles(filenames));
        return zipFile.getFile();
    }

    private void createDirectory() {
        try {
            Files.createDirectory(this.rootLocation, DEFAULT_PERMISSIONS);
        } catch(FileAlreadyExistsException ignored){
        } catch (IOException e) {
            throw new RuntimeException("failed to create directory", e);
        }
    }

    public void clearAllFiles() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
        createDirectory();
    }

}
