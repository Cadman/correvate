package com.dcadman.zipper.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.HashMap;

import static java.lang.String.format;

public class UnzippedFileMatcher extends TypeSafeMatcher<HashMap<String, byte[]>> {

    private final String filename;
    private final byte[] file;

    public UnzippedFileMatcher(String filename, byte[] file) {
        this.filename = filename;
        this.file = file;
    }

    @Override
    protected void describeMismatchSafely(HashMap<String, byte[]> item, Description mismatchDescription) {
        mismatchDescription.appendText(format("Unzipped file %s was not the same as the upload file", filename));
    }

    @Override
    protected boolean matchesSafely(HashMap<String, byte[]> unzipped) {
        return byteArraysEqual(unzipped.get(filename), file);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(format("File: %s ", file));
    }

    public static boolean byteArraysEqual(byte[] array1, byte[] array2) {
        if(array1.length != array2.length) return false;
        for(int i = 0; i < array1.length; i++) {
            if(array1[i] != array2[i]) return false;
        }
        return true;
    }

    public static UnzippedFileMatcher hasFileEqualTo(String filename, byte[] file) {
        return new UnzippedFileMatcher(filename, file);
    }
}
