package com.dcadman.zipper.controllers;

import com.dcadman.zipper.services.FileStorageService;
import net.lingala.zip4j.ZipFile;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;

import static com.dcadman.zipper.matchers.UnzippedFileMatcher.hasFileEqualTo;
import static java.lang.String.format;
import static java.nio.file.Files.readAllBytes;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FileZipperControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FileStorageService fileStorageService;

    private final FileStorageService localFileStorageService = new FileStorageService("test-unzipped");

    private static MockMultipartFile MOCK_FILE_1;
    private static MockMultipartFile MOCK_FILE_2;

    private static final String FILENAME_1 = "test-file-1.txt";
    private static final String FILENAME_2 = "test-file-2.txt";
    private static final String TEST_DIR = "test-unzipped";

    @BeforeEach
    void setUp() {
        fileStorageService.clearAllFiles();
        localFileStorageService.clearAllFiles();
    }

    @AfterEach
    void tearDown() {
        fileStorageService.clearAllFiles();
        localFileStorageService.clearAllFiles();
    }

    @BeforeAll
    static void beforeAll() throws IOException {
        final byte[] uploadFile1 = Thread.currentThread().getContextClassLoader().getResourceAsStream(FILENAME_1).readAllBytes();
        final byte[] uploadFile2 = Thread.currentThread().getContextClassLoader().getResourceAsStream(FILENAME_2).readAllBytes();
        MOCK_FILE_1 = new MockMultipartFile("data", FILENAME_1, "text/plain",uploadFile1);
        MOCK_FILE_2 = new MockMultipartFile("data", FILENAME_2, "text/plain", uploadFile2);
    }

    @Test
    public void uploadAndReturnSameFile() throws Exception {
        final MvcResult result = mockMvc.perform(
                multipart("/zip-files")
                        .file(MOCK_FILE_1)
                        .file(MOCK_FILE_2)
                        .accept("application/zip")
        ).andExpect(status().isOk()).andReturn();

        assertThat(extractZippedFiles(result),
                allOf(
                        hasFileEqualTo(MOCK_FILE_1.getOriginalFilename(), MOCK_FILE_1.getBytes()),
                        hasFileEqualTo(MOCK_FILE_2.getOriginalFilename(), MOCK_FILE_2.getBytes())));
    }

    @Test
    public void emptyUploadFails() {
        assertThrows(FileNotFoundException.class, () -> mockMvc.perform(
                post("/zip-files")
        ).andReturn());
    }

    private HashMap<String, byte[]> extractZippedFiles(MvcResult result) throws IOException {
        writeFileToTestDir(result);
        ZipFile zipFile = new ZipFile(TEST_DIR + "/test-file.zip");
        zipFile.extractAll(TEST_DIR);
        HashMap<String, byte[]> files = new HashMap<>();
        files.put(MOCK_FILE_1.getOriginalFilename(),
                readAllBytes(Paths.get(format("%s/%s", TEST_DIR,FILENAME_1))));
        files.put(MOCK_FILE_2.getOriginalFilename(),
                readAllBytes(Paths.get(format("%s/%s", TEST_DIR,FILENAME_2))));
        return files;
    }

    private void writeFileToTestDir(MvcResult result) throws IOException {
        byte[] contentAsByteArray = result.getResponse().getContentAsByteArray();
        FileOutputStream fos = new FileOutputStream(TEST_DIR + "/test-file.zip");
        fos.write(contentAsByteArray);
        fos.close();
    }

}