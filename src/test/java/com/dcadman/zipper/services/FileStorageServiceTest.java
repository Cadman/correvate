package com.dcadman.zipper.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class FileStorageServiceTest {

    FileStorageService fileStorageService = new FileStorageService("test-upload-dir");
    private String TEST_FILE_NAME = "test-filename.txt";

    @BeforeEach
    void setUp() {
        fileStorageService.clearAllFiles();
    }

    @AfterEach
    void tearDown() {
        fileStorageService.clearAllFiles();
    }

    @Test
    public void canSaveFile() throws IOException {
        byte[] fileSaveData = {11};
        fileStorageService.saveFile(wrapTestDataInFile(fileSaveData));

        assertThat(fetchSavedRawData(), equalTo(fileSaveData));
    }

    private byte[] fetchSavedRawData() throws IOException {
        return Files.readAllBytes(fileStorageService.getFile(TEST_FILE_NAME).toPath());
    }

    private MockMultipartFile wrapTestDataInFile(byte[] testData) {
        return new MockMultipartFile("file", TEST_FILE_NAME, "text/plain", testData);
    }

    @Test
    public void returnsAnEmptyFileIfItDoesNotExist() {
        assertFalse(fileStorageService.getFile(TEST_FILE_NAME).isFile());
    }

}